all: lint test

test:
	pytest-3

lint:
	pycodestyle .
	pylint3 src/*.py 
